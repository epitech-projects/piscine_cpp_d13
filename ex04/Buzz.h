/*
** Buzz.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex02
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Jan 20 16:07:00 2014 Jean Gravier
** Last update Tue Jan 21 00:59:13 2014 Jean Gravier
*/

#ifndef _BUZZ_H_
#define _BUZZ_H_

#include "Toy.h"
#include <string>

class			Buzz: public Toy
{
 public:
  Buzz(std::string const&, std::string const& = "buzz.txt");
  ~Buzz();

 public:
  bool			speak(std::string const&);
};

#endif /* _BUZZ_H_ */
