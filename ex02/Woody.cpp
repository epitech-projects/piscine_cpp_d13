//
// Woody.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 20 16:07:07 2014 Jean Gravier
// Last update Mon Jan 20 16:16:38 2014 Jean Gravier
//

#include <string>
#include "Toy.h"
#include "Woody.h"

Woody::Woody(std::string const& name, std::string const& file): Toy(Toy::WOODY, name, file)
{

}

Woody::~Woody()
{

}

