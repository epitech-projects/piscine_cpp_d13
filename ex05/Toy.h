/*
** Toy.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Jan 20 13:04:28 2014 Jean Gravier
** Last update Mon Jan 20 18:34:40 2014 Jean Gravier
*/

#ifndef _TOY_H_
#define _TOY_H_

#include <string>
#include <fstream>
#include <iostream>
#include "Picture.h"

class			Toy
{
 public:
  typedef enum { BASIC_TOY, ALIEN, BUZZ, WOODY } ToyType;
  class				Error
    {
    public:
      typedef enum { UNKNOWN, PICTURE, SPEAK } ErrorType;

    public:
      Error();
      ~Error();

    public:
      std::string const&	what() const;
      std::string const&	where() const;

      std::string		_what;
      std::string		_where;
      ErrorType			type;
    };

 public:
  Toy(ToyType, std::string const&, std::string const&);
  Toy(Toy const&);
  Toy();
  virtual ~Toy();

 public:
  ToyType		getType() const;
  std::string const&	getName() const;
  std::string const&	getAscii() const;
  Error const&		getLastError() const;

 public:
  void			setName(std::string const&);
  bool			setAscii(std::string const&);
  Toy			&operator=(Toy const&);
  virtual void		speak(std::string const&);
  virtual bool		speak_es(std::string const&);
  void			operator<<(std::string const&);

 protected:
  ToyType		_type;
  std::string		_name;
  Picture		_picture;
  Error			_error;
};

std::ostream		&operator<<(std::ostream&, Toy const&);

#endif /* _TOY_H_ */
