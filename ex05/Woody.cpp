//
// Woody.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 20 16:07:07 2014 Jean Gravier
// Last update Mon Jan 20 18:34:06 2014 Jean Gravier
//

#include <string>
#include "Toy.h"
#include "Woody.h"

Woody::Woody(std::string const& name, std::string const& file): Toy(Toy::WOODY, name, file)
{

}

Woody::~Woody()
{

}

void		Woody::speak(std::string const& statement)
{
  std::cout << "WOODY: " << this->_name << " \"" << statement << "\"" << std::endl;
}

bool		Woody::speak_es(std::string const& statement)
{
  (void)statement;
  this->_error._what = "wrong mode";
  this->_error._where = "speak_es";
  this->_error.type = Toy::Error::SPEAK;
  return (false);
}
