/*
** Picture.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Jan 20 12:23:09 2014 Jean Gravier
** Last update Mon Jan 20 12:58:41 2014 Jean Gravier
*/

#ifndef _PICTURE_H_
#define _PICTURE_H_

#include <string>
#include <iostream>

class			Picture
{
 public:
  Picture(std::string const&);
  Picture();
  ~Picture();

 public:
  bool			getPictureFromFile(std::string const&);

 public:
  std::string		data;

 private:
  std::string		_file;
};

#endif /* _PICTURE_H_ */
