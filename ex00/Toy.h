/*
** Toy.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Jan 20 13:04:28 2014 Jean Gravier
** Last update Mon Jan 20 16:53:58 2014 Jean Gravier
*/

#ifndef _TOY_H_
#define _TOY_H_

#include <string>
#include "Picture.h"

class			Toy
{
 public:
  typedef enum { BASIC_TOY, ALIEN } ToyType;

 public:
  Toy(ToyType, std::string const&, std::string const&);
  Toy();
  ~Toy();

 public:
  ToyType		getType() const;
  std::string const&	getName() const;
  std::string const&	getAscii() const;

 public:
  void			setName(std::string const&);
  bool			setAscii(std::string const&);

 private:
  ToyType		_type;
  std::string		_name;
  Picture		_picture;
};

#endif /* _TOY_H_ */
