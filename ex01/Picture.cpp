//
// Picture.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 20 10:43:06 2014 Jean Gravier
// Last update Mon Jan 20 16:00:36 2014 Jean Gravier
//

#include "Picture.h"
#include <string>
#include <fstream>

Picture::Picture(std::string const& file): _file(file)
{
  std::ifstream		stream;
  char			c;

  stream.open(file.c_str(), std::ios::in);
  if (stream)
    {
      this->data = "";
      while (stream.get(c))
	{
	  this->data += c;
	}
    }
  else
    this->data = "ERROR";
}

Picture::Picture()
{
  this->data = "";
}

Picture::~Picture()
{

}

Picture::Picture(Picture const& picture)
{
  this->_file = picture._file;
  this->data = picture.data;
}

Picture		&Picture::operator=(Picture const& picture)
{
  this->_file = picture._file;
  this->data = picture.data;
  return (*this);
}

bool			Picture::getPictureFromFile(std::string const& file)
{
  std::ifstream		stream;
  char			c;

  stream.open(file.c_str(), std::ios::in);
  if (stream)
    {
      this->data = "";
      while (stream.get(c))
	{
	  this->data += c;
	}
      return (true);
    }
  else
    {
      this->data = "ERROR";
      return (false);
    }
}
