/*
** Woody.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex02
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Jan 20 16:07:00 2014 Jean Gravier
** Last update Mon Jan 20 16:34:50 2014 Jean Gravier
*/

#ifndef _WOODY_H_
#define _WOODY_H_

#include "Toy.h"

class			Woody: public Toy
{
 public:
  Woody(std::string const&, std::string const& = "woody.txt");
  ~Woody();

 public:
  void			speak(std::string const&);
};

#endif /* _WOODY_H_ */
