//
// Toy.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 20 13:04:23 2014 Jean Gravier
// Last update Mon Jan 20 17:07:43 2014 Jean Gravier
//

#include <string>
#include "Picture.h"
#include "Toy.h"

Toy::Toy(ToyType type, std::string const& name, std::string const& file): _type(type), _name(name)
{
  this->_picture = Picture(file);
}

Toy::Toy()
{
  this->_type = Toy::BASIC_TOY;
  this->_name = "toy";
}

Toy::Toy(Toy const& toy)
{
  this->_type = toy._type;
  this->_name = toy._name;
  this->_picture = toy._picture;
}

Toy::~Toy()
{

}

Toy		&Toy::operator=(Toy const& toy)
{
  this->_type = toy._type;
  this->_name = toy._name;
  this->_picture = toy._picture;
  return (*this);
}

Toy::ToyType		Toy::getType() const
{
  return (this->_type);
}

std::string const&	Toy::getName() const
{
  return (this->_name);
}

std::string const&	Toy::getAscii() const
{
  return (this->_picture.data);
}

void			Toy::setName(std::string const& name)
{
  this->_name = name;
}

bool			Toy::setAscii(std::string const& file)
{
  if (this->_picture.getPictureFromFile(file))
    return (true);
  else
    return (false);
}

void			Toy::speak(std::string const& statement)
{
  std::cout << this->_name << " \"" << statement << "\"" << std::endl;
}
