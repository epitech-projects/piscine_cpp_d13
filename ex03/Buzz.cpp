//
// Buzz.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d13/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 20 16:07:07 2014 Jean Gravier
// Last update Mon Jan 20 16:43:56 2014 Jean Gravier
//

#include <string>
#include "Toy.h"
#include "Buzz.h"

Buzz::Buzz(std::string const& name, std::string const& file): Toy(Toy::BUZZ, name, file)
{

}

Buzz::~Buzz()
{

}

void		Buzz::speak(std::string const& statement)
{
  std::cout << "BUZZ: " << this->_name << " \"" << statement << "\"" << std::endl;
}
